package com.uni.aopdemo.controller;

import com.uni.aopdemo.model.Employee;
import com.uni.aopdemo.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class EmployeeController {

    private final EmployeeService employeeService;

    @GetMapping("/add/employee")
    public Employee addEmployee(@RequestParam("name") String name, @RequestParam("empId") String empId) {
        Employee employee = employeeService.createEmployee(name, empId);
        employee.getName();
        employee.getEmpId();

        employeeService.newMethod();

        return employee;
    }

    @GetMapping("/remove/employee")
    public String removeEmployee( @RequestParam("empId") String empId) {

        employeeService.deleteEmployee(empId);

        return "Employee removed";
    }
}
