package com.uni.aopdemo.model;

import com.uni.aopdemo.annotations.Loggable;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Component;

@Component
@Configurable
public class Employee {

    private String empId;
    private String name;

    public String getName() {
        return name;
    }

    public String getEmpId() {
        return empId;
    }

    @Loggable
    public void setEmpId(String empId) {
        this.empId = empId;
    }

    @Loggable
    public void setName(String name) {
        this.name = name;
    }

    public void throwException() {
        throw new RuntimeException("Dummy Exception");
    }
}
