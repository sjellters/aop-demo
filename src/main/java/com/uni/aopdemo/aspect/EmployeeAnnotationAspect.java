package com.uni.aopdemo.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class EmployeeAnnotationAspect {

    @Before("@annotation(com.uni.aopdemo.annotations.Loggable)")
    public void onLoggable() {
        System.out.println("Executing loggable advice!");
    }
}
