package com.uni.aopdemo.service;

import com.uni.aopdemo.model.Employee;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {

    public Employee createEmployee(String name, String empId) {
        Employee employee = new Employee();
        employee.setName(name);
        employee.setEmpId(empId);

        employee.getName();
        return employee;
    }

    public void deleteEmployee(String empId) {
    }

    public void newMethod() {

    }
}
